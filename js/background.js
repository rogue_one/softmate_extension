'use strict';

class MessageManager {

	constructor() {
		this.infoHere = 'http://www.softomate.net/ext/employees/list.json';
		this.hosts = []; // hosts list
		this.interval = 3600000; // 1 hour
		this.showLimit = 3; // msg show limit
		this.checkpoint = 'softmateCheckpoint'; // data update timestamp
		this.hostsStorage = 'softmateHosts'; // hosts 
		this.alarmName = 'softmateAlarm'; // alarm
		this.counters = {}; // msg showing counters

		let self = this;
		// load website info from chrome local storage if exists
		chrome.storage.local.get(self.hostsStorage, function (data) { // get data from storage without loading json
			if (data.hasOwnProperty([self.hostsStorage])) {
				self.hosts = data[self.hostsStorage];
				for (let host of self.hosts) {
					self.counters[host.domain] = 0; // init counters
				}
			} else {
				self.loadInfo(); // let's load json
			}
		});
	}

	// load json with hosts info
	loadInfo() {
		let self = this;
		$.ajax({
			url: this.infoHere, // load json
			success: function (data) { // ok! let's do it
				self.hosts = data;
				if (Object.keys(self.counters).length == 0) { // omg! counters are not initialized
					for (let host of self.hosts) {
						self.counters[host.domain] = 0; // then do it now!
					}
				}
				// save info about hosts  in chrome local storage
				chrome.storage.local.set({[self.hostsStorage]: self.hosts}, function () { // sava data from json to storage
				});
			}
		});
	}

	start() {
		let self = this;
		chrome.storage.local.get(this.checkpoint, function (data) { // get last timestamp
			let checkpoint;
			if (data.hasOwnProperty([self.checkpoint])) { // check timestamp exists
				checkpoint = data[self.checkpoint]; // OK
			} else {
				checkpoint = {}; // no timestamp there
			}
			if (checkpoint.time === undefined || Date.now() - checkpoint.time > self.interval) { // or we didnt find timestamp or it happened a long time ago
				checkpoint.time = Date.now(); // new timestamp
				chrome.storage.local.set({[self.checkpoint]: checkpoint}, function () { // save it
				});
			}
			chrome.alarms.create(self.alarmName, { // we should reload json times per interval
				delayInMinutes: ((self.interval)/60000) - (Date.now() - checkpoint.time)/60000,
				periodInMinutes: (self.interval)/60000
			})
			chrome.alarms.onAlarm.addListener(function (alarm) {
				if (alarm.name === self.alarmName) { // it's time
					console.log("ALARM");
					self.loadInfo(); // reload json
				}
			});
		});

		chrome.runtime.onMessage.addListener(
			function(request, sender, sendResponse) {
				try {
					if (request.action === 'reload') { // reload page
						sendResponse(self.decide(request.domain, 1));
					} else if (request.action === 'close') { // close our message
						sendResponse(self.decide(request.domain, 4));
					} else { // oops
						throw new Error('Incorrect request');
					}
				} catch (e) {
					console.log(e);
				}
			});	
	}

	decide (domain, n) {
		let host = this.checkDomain(domain)
		if (host === undefined)
			return { response: 0 };
		try {
			if (this.checkCounter(host.domain, n))
				return {
					response: 1,
					msg: host.message
				};
		} catch (e) {
			console.log(e);
		}
		return { response: 0 };
	}

	checkDomain(domain) {
		for (let host of this.hosts) {
			if (host.domain === domain.replace('www.', ''))
				return host;
		}
		return undefined;
	}

	checkCounter(domain, n) {
		return this.counters[domain] <= this.showLimit && // stop endless counter increment
			  (this.counters[domain] += n) <= this.showLimit; // increment and compare
	}
}

var manager = new MessageManager();
manager.start();
