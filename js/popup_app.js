'use strict';

var eBus = new Vue(); // event bus

Vue.component('host', {
	template: `
		<div class="host" :style="host_style">
			<a href="http://{{host.domain}}" target="_blank">
				{{ host.name }}
			</a>
		</div>
	`,
	props: ['host', 'row'],
	computed: {
		host_style: function () {
			return {
				gridRow: `${this.row}`,
			}
		}
	}
});

Vue.component('host_list', {
	template: `
		<div id="hosts" :style="hosts_style">
			<template v-for="(index, host) in hosts">
				<host :host="host" :row="row(index)"></host>
			</template>
		</div>
	`,
	props: ['hosts'],
	computed: {
		hosts_style: function () {
			return {
				gridRow: '3',
				display: 'grid',
				gridTemplateRows: `1fr repeat(${this.hosts.length}, max-content) 1fr`,
				mixHeight: '0',
				minWidth: '0',
			}
		},
	},
	methods: {
		row: function (index) {
			console.log(index);
			return index + 2;
		},
	},
});

let popup_app = new Vue({
	el: '#popup_app',
	data: {
		hosts: [],
	},
	methods: {
	},
	computed: {
	},
	created: function () {
		let self = this;
		eBus.$on('hosts-update', function (hosts) {
			console.log('receive: hosts-update');
			self.hosts = hosts;
			for (let host of hosts) {
				console.log(JSON.stringify(host));
			}
		})
	},
});
