'use strict';

class Popup {
	constructor () {
		this.hostsStorage = 'softmateHosts';
		this.hosts = [];
		this.counter = 0;

		let self = this;
		chrome.storage.local.get(self.hostsStorage, function (data) {
			if (data.hasOwnProperty([self.hostsStorage])) {
				self.hosts = data[self.hostsStorage];
				self.UpdateDOM();
			}
		});
	}

	Start() {
		let self = this;
		chrome.storage.onChanged.addListener(function(changes, namespace) {
			//for (let key in changes) {
			let key = self.hostsStorage;
			if (key in changes) {
				var storageChange = changes[key];
				let hosts = data[self.hostsStorage];
				self.UpdateDOM();
			}
		});
	}

	UpdateDOM() {
		console.log('send: hosts-update');
		eBus.$emit('hosts-update', this.hosts);
	}
}

var popup = new Popup();

$(function () {
	popup.Start();
});
