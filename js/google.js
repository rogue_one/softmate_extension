'use strict';

class SomeClass {
	constructor () {
		this.hostsStorage = 'softmateHosts';
		this.hosts = [];
		this.extraStyle =  document.createElement('div');
		this.iconURL = chrome.extension.getURL('img/icon.png');

		let self = this;
		chrome.storage.local.get(self.hostsStorage, function (data) {
			let hosts;
			if (data.hasOwnProperty([self.hostsStorage])) {
				hosts = data[self.hostsStorage];
				self.hosts = hosts;
				self.UpdateDOM();
			}
		});
	}

	Go () {
		document.body.append(this.extraStyle);
		let self = this;
		chrome.storage.onChanged.addListener(function(changes, namespace) {
			let key = self.hostsStorage;
			if (key in changes) {
				var storageChange = changes[key];
				self.UpdateDOM();
			}
		});
	}

	UpdateDOM () {
		let innerHTML = `<style>`;
		try {
			for (let i in this.hosts) {
				let host = this.hosts[i];
				innerHTML += `h3.r>a[href*="${host.domain}"]:before`;
				if (i < this.hosts.length - 1)
					innerHTML += `,`;
			}
		} catch (e) {
			console.log(e);
		}
		innerHTML += `
				{
					content: '';
					background: black;
					background: url(${this.iconURL});
					background-size: contain;
					position: absolute;
					left: -35px; 
					width: 30px;
					height: 30px;
					border-radius: 50%;
					top: 0;
					overflow: hidden;
				}
			</style>
		`;
		this.extraStyle.innerHTML = innerHTML;
	}
}


$(function () {
	let someObject = new SomeClass();
	someObject.Go();
});
