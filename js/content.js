'use strict';

class MessageDisplay {

	constructor() {
		this.notification = document.createElement("div"); // DOM to injection
		this.html_template = chrome.extension.getURL('html/content.html');
		this.mustache_template = chrome.extension.getURL('templates/content.mst');
		this.font_awesome_css = chrome.extension.getURL('css/font-awesome.min.css');
		this.primary_css = chrome.extension.getURL('css/content.css');
		this.aller_font = chrome.extension.getURL('css/fonts/style.css');
	}

	start() {
		this.request('reload');
	}

	request(action) { // to show or not to show
		let self = this;
		chrome.runtime.sendMessage({domain: window.location.host, action: action}, function(response) { // ask background script about it
			try {
				if (response.response) // response == 1
					self.open(response.msg); // open message
			} catch (e) {
				console.log(e);
			}
		});
	}

	open (msg) { // inject message
		let self = this;

		$('<link>') // inject font-awesome
			.appendTo('head')
			.attr({
				type: 'text/css',
				rel: 'stylesheet',
				href: this.font_awesome_css,
			});

		$('<link>') // inject styles
			.appendTo('head')
			.attr({
				type: 'text/css',
				rel: 'stylesheet',
				href: this.primary_css,
			});

		$.get(this.html_template, function (data) { // get html template
			$(data).appendTo('body'); // inject it
			$.get(self.mustache_template, function(template) { // get mustache template
				var rendered = Mustache.render(template, {msg: msg}); // render html
				$('#softmate_message').html(rendered); // inject it
			});
		});
	}

	close () { // close message
		$('#softmate_message').remove(); // remove message
		this.request('close');
	}
}

let display = new MessageDisplay();
$(function () {
	display.start();
	$(document).on('click', '#close_softmate_message', function () {
		display.close();
	});
});
